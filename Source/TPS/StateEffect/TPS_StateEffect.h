// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "TPS_StateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TPS_API UTPS_StateEffect : public UObject
{
	GENERATED_BODY()

public:
	virtual bool InitObject(AActor* Actor, FName NameBoneHit);
	virtual void DestroyObject();

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Setting")
		bool bIsStakable = false;

	AActor* myActor = nullptr;
};

UCLASS()
class TPS_API UTPS_StateEffect_ExecuteOnce : public UTPS_StateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Setting Execute Once")
		float Power = 20.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Setting Execute Once")
		UParticleSystem* ParticleEffect = nullptr;
	UParticleSystemComponent* ParticleEmitter = nullptr;
	FTimerHandle TimerHandle_EffectDestroyTimer;
};

UCLASS()
class TPS_API UTPS_StateEffect_ExecuteTimer : public UTPS_StateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Setting Execute Once")
		float Power = 20.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Setting Execute Once")
		float Timer = 5.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Setting Execute Once")
		float RateTime = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Setting Execute Once")
	UParticleSystem* ParticleEffect = nullptr;
	UParticleSystemComponent* ParticleEmitter = nullptr;
};