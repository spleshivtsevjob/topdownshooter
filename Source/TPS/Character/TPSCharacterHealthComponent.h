// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPSHealthComponent.h"
#include "TPSCharacterHealthComponent.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS()
class TPS_API UTPSCharacterHealthComponent : public UTPSHealthComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChange OnShieldChange;

	FTimerHandle TimerHandle_CollDownShieldTimer;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

protected:
	float Shield = 100.0f;

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float CollDownShieldRecoveryTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoveryValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoveryRate = 0.1f;

	void ChangeHealthValue(float ChangeValue) override;
	UFUNCTION(BlueprintCallable, Category = "Shield")
	float GetCurrentShield();

	void ChangeShieldValue(float ChangeValue);
	void CollDownShieldEnd();
	void RecoveryShield();

	UFUNCTION(BlueprintCallable)
		float GetShieldValue();
};
